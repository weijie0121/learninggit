/**
 * 
 */
package edu.ucsd.cs110w.temperature;

/**
 * @author WeiJie
 *
 */
public class Kelvin extends Temperature
{
	public Kelvin(float t)
	{
		super(t);
	}
	public String toString()
	{
		String s = this.getValue() + " K";
		return s;
	}
	@Override
	public Temperature toCelsius() {
		float input = this.getValue() - 273;
		return new Celsius(input);
	}
	public Temperature toFahrenheit() {
		float input = (this.getValue() - 273) * 9 / 5 + 32;
		return new Fahrenheit(input);
	}
	@Override
	public Temperature toKelvin() {
		float input = this.getValue();
		return new Kelvin(input);
	}
}