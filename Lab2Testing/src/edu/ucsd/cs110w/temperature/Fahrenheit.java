/**
 * 
 */
package edu.ucsd.cs110w.temperature;

/**
 * @author WeiJie
 *
 */
public class Fahrenheit extends Temperature
{
	public Fahrenheit(float t)
	{
		super(t);
	}
	public String toString()
	{
		String s = this.getValue() + " F";
		return s;
	}
	@Override
	public Temperature toCelsius() {
		float input = (this.getValue() - 32) * 5 / 9;
		return new Celsius(input);
	}
	@Override
	public Temperature toFahrenheit() {
		float input = this.getValue();
		return new Fahrenheit(input);
	}
	@Override
	public Temperature toKelvin() {
		float input = (this.getValue() - 32) * 5 / 9 + 273;
		return new Kelvin(input);
	}
}
