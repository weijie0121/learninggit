/**
 * 
 */
package edu.ucsd.cs110w.temperature;

/**
 * @author WeiJie
 *
 */
public class Celsius extends Temperature
{
	public Celsius(float t)
	{
		super(t);
	}
	public String toString()
	{
		String s = this.getValue() + " C";
		return s;
	}
	@Override
	public Temperature toCelsius() {
		float input = this.getValue();
		return new Celsius(input);
	}
	@Override
	public Temperature toFahrenheit() {
		float input = this.getValue() * 9 / 5 + 32;
		return new Fahrenheit(input);
	}
	@Override
	public Temperature toKelvin() {
		float input = this.getValue() + 273;
		return new Kelvin(input);
	}
}
